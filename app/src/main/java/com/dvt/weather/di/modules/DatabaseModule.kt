package com.dvt.weather.di.modules

import android.app.Application
import com.dvt.weather.Database
import com.dvt.weather.data.queries.WeatherQueries
import com.squareup.sqldelight.android.AndroidSqliteDriver
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Singleton
    @Provides
    fun provideDatabase(application: Application): Database {
        val driver = AndroidSqliteDriver(
            schema = Database.Schema,
            context = application,
            name = "weather.db"
        )
        return Database.invoke(driver)
    }

    @Provides
    fun provideWeatherQueries(database: Database): WeatherQueries {
        return database.weatherQueries
    }
}