package com.dvt.weather.ui.main

import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.location.Location
import android.os.Bundle
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dvt.weather.R
import com.dvt.weather.data.entities.*
import com.dvt.weather.data.network.dto.ForecastResponse
import com.dvt.weather.domain.result.Result
import com.dvt.weather.ui.base.BaseActivity
import com.dvt.weather.ui.theme.GeneralTheme
import com.dvt.weather.ui.theme.Theme
import com.dvt.weather.ui.theme.Theme.*
import com.dvt.weather.ui.theme.WeatherTheme
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionsRequired
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private val mainViewModel: MainViewModel by viewModels()

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var currentLocation: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun retrieveLocation(onLocationRetrieved: () -> Unit) {
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            currentLocation = location
            onLocationRetrieved()
        }
    }

    private fun refreshWeather() {
        if (currentLocation != null) {
            mainViewModel.getCurrentWeather(
                lat = currentLocation!!.latitude,
                lon = currentLocation!!.longitude
            )
            mainViewModel.getForecast5(
                lat = currentLocation!!.latitude,
                lon = currentLocation!!.longitude
            )
        }
    }

    @ExperimentalPermissionsApi
    @Composable
    override fun Content() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        val locationPermissionsState =
            rememberMultiplePermissionsState(
                permissions = listOf(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
        var showPermissionsRationale by rememberSaveable { mutableStateOf(true) }
        PermissionsRequired(
            multiplePermissionsState = locationPermissionsState,
            permissionsNotGrantedContent = {
                GeneralTheme {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Text(
                            text = "We will need to access your location to show your current and weather forecast.",
                            textAlign = TextAlign.Center,
                            fontSize = 18.sp,
                            color = MaterialTheme.colors.onPrimary
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Row {
                            Button(onClick = { locationPermissionsState.launchMultiplePermissionRequest() }) {
                                Text("Allow us to access your location.")
                            }
                        }
                    }
                }
            },
            permissionsNotAvailableContent = {
                GeneralTheme {
                    Column {
                        Text(
                            "We can not get your current location to display current and weather forecast."
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                    }
                }
            }
        ) {
            retrieveLocation {
                refreshWeather()
            }
            MainScreen(mainViewModel.loading,
                mainViewModel.currentWeather.observeAsState(Result.Loading),
                mainViewModel.forecast5.observeAsState(Result.Loading),
                refresh = {
                    refreshWeather()
                }
            )
        }
    }
}


@Composable
fun MainScreen(
    loadingState: MutableState<Boolean>,
    currentWeatherState: State<Result<CurrentWeather>>,
    forecastWeatherState: State<Result<ForecastResponse>>,
    refresh: () -> Unit
) {
    val theme = remember {
        mutableStateOf(CLOUDY)
    }
    if (currentWeatherState.value is Result.Success) {
        val currentWeather = (currentWeatherState.value as Result.Success).data
        val weather = currentWeather.weather.first()
        theme.value = when {
            weather.isSunny() -> {
                SUNNY
            }
            weather.isCloudy() -> {
                CLOUDY
            }
            weather.isRainy() -> {
                RAINY
            }
            else -> SUNNY
        }
    }
    WeatherTheme(theme = theme.value) {
        val scaffoldState = rememberScaffoldState()
        Scaffold(
            scaffoldState = scaffoldState,
            content = {
                val systemUiController = rememberSystemUiController()
                val navigationBarColor = MaterialTheme.colors.primary
                SideEffect {
                    systemUiController.setStatusBarColor(
                        color = theme.value.statusBarColor,
                        darkIcons = false
                    )
                    systemUiController.setNavigationBarColor(
                        color = navigationBarColor,
                        darkIcons = false
                    )
                }
                Surface(color = MaterialTheme.colors.primary) {
                    Box(modifier = Modifier.fillMaxSize()) {
                        SwipeRefresh(
                            state = rememberSwipeRefreshState(isRefreshing = loadingState.value),
                            onRefresh = {
                                refresh()
                            }) {
                            Column {
                                CurrentWeatherScreen(theme, currentWeatherState)
                                Divider(
                                    color = Color.White,
                                    thickness = 1.dp
                                )
                                ForecastWeatherScreen(forecastWeatherState)
                            }
                        }

                        IconButton(onClick = {
                            theme.value = when (theme.value) {
                                SUNNY -> {
                                    CLOUDY
                                }
                                CLOUDY -> {
                                    RAINY
                                }
                                RAINY -> {
                                    SUNNY
                                }
                            }
                        }) {
                            Icon(
                                imageVector = Icons.Default.Menu,
                                contentDescription = null,
                                modifier = Modifier.padding(horizontal = 12.dp),
                                tint = MaterialTheme.colors.onPrimary
                            )
                        }
                    }
                }
            }
        )
    }
}

@Composable
fun CurrentWeatherScreen(
    theme: MutableState<Theme>,
    currentWeatherState: State<Result<CurrentWeather>>
) {
    val averageTemp = remember {
        mutableStateOf(0f)
    }
    val minimumTemp = remember {
        mutableStateOf(0f)
    }
    val maximumTemp = remember {
        mutableStateOf(0f)
    }
    val weatherDescription = remember {
        mutableStateOf("")
    }
    val cachedAt = remember {
        mutableStateOf<Date?>(null)
    }
    if (currentWeatherState.value is Result.Success) {
        val result = (currentWeatherState.value as Result.Success)
        val currentWeather = result.data
        val main = currentWeather.main
        val weather = currentWeather.weather.first()
        averageTemp.value = main.temp
        minimumTemp.value = main.tempMin
        maximumTemp.value = main.tempMax
        if(result.cachedAt != null) {
            cachedAt.value = Date(result.cachedAt)
        } else {
            cachedAt.value = null
        }
        weatherDescription.value = weather.name()
    }
    Column {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.45f)
        ) {
            Image(
                painter = painterResource(id = theme.value.art),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(),
                contentScale = ContentScale.FillBounds
            )
            Column(
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .padding(64.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.high) {
                    Text(
                        text = "${averageTemp.value}°",
                        style = MaterialTheme.typography.h2
                    )
                    Text(
                        text = weatherDescription.value,
                        style = MaterialTheme.typography.h4
                    )
                }
                if(cachedAt.value != null) {
                    CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.disabled) {
                        Text(
                            text ="Updated at ${DateFormat.getTimeInstance().format(cachedAt.value!!)}",
                            style = MaterialTheme.typography.caption
                        )
                    }
                }
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.Start
            ) {
                Text(
                    text = "${minimumTemp.value}°",
                    style = MaterialTheme.typography.subtitle1
                )
                Text(text = "min")
            }
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "${averageTemp.value}°", style = MaterialTheme.typography.subtitle1)
                Text(text = "Current")
            }
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.End
            ) {
                Text(text = "${maximumTemp.value}°", style = MaterialTheme.typography.subtitle1)
                Text(text = "max")
            }
        }
    }
}

@Composable()
fun ForecastWeatherScreen(forecastWeatherState: State<Result<ForecastResponse>>) {
    val dailyForecast = mutableMapOf<Date, List<ForecastWeather>>()
    when (forecastWeatherState.value) {
        is Result.Success -> {
            val forecastResponse = (forecastWeatherState.value as Result.Success).data
            val forecastWeather = forecastResponse.list
                .filter {
                    val calendar = Calendar.getInstance()
                    calendar.set(Calendar.HOUR_OF_DAY, 0)
                    calendar.set(Calendar.MINUTE, 0)
                    calendar.set(Calendar.SECOND, 0)
                    calendar.set(Calendar.MILLISECOND, 0)
                    calendar.add(Calendar.DAY_OF_MONTH, 1)
                    it.dtTxt.after(calendar.time)
                }
            dailyForecast.putAll(forecastWeather
                .groupBy {
                    val calendar = Calendar.getInstance()
                    calendar.time = it.dtTxt
                    calendar.set(Calendar.HOUR_OF_DAY, 0)
                    calendar.set(Calendar.MINUTE, 0)
                    calendar.set(Calendar.SECOND, 0)
                    calendar.set(Calendar.MILLISECOND, 0)
                    calendar.time
                }
            )
        }
        is Result.Error -> {
        }
        else -> {
        }
    }
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(dailyForecast.keys.toList()) { date ->
            val forecastWeather = dailyForecast[date]!![0]
            DayForecastRow(date, forecastWeather)
        }
    }
}

@Composable
fun DayForecastRow(date: Date, forecast: ForecastWeather) {
    val weather = forecast.weather[0]
    val averageTempState = remember {
        mutableStateOf(forecast.main.temp)
    }
    val weatherIconState = remember {
        mutableStateOf(R.drawable.rain)
    }
    weatherIconState.value = when {
        weather.isCloudy() -> R.drawable.partlysunny
        weather.isSunny() -> R.drawable.clear
        else -> {
            R.drawable.rain
        }
    }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(0.dp, 8.dp, 0.dp, 8.dp),
    ) {
        Text(
            text = SimpleDateFormat("EEEE").format(date),
            modifier = Modifier.weight(1f)
        )
        Column(
            modifier = Modifier.weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = weatherIconState.value),
                contentDescription = null,
                modifier = Modifier.size(24.dp),
                contentScale = ContentScale.FillBounds
            )
        }
        Column(
            modifier = Modifier.weight(1f),
            horizontalAlignment = Alignment.End
        ) {
            Text(
                text = "${averageTempState.value}°",
            )
        }
    }
}

@Preview(
    name = "Main Screen",
    showSystemUi = true,
    uiMode = UI_MODE_NIGHT_NO
)
@Composable
fun PreviewMainScreen() {
    val loading = remember {
        mutableStateOf(true)
    }
    val currentWeatherResult: Result<CurrentWeather> = Result.Success(
        CurrentWeather(
            coord = Coordinate(36.8652, -1.221),
            name = "Kiambu",
            base = "",
            clouds = Cloud(all = 62),
            cod = 200,
            timezone = 10800,
            id = 192710,
            dt = 1627849222,
            main = Main(
                temp = 16.41f,
                feelsLike = 15.93f,
                tempMin = 16.41f,
                tempMax = 16.41f,
                pressure = 1021,
                humidity = 70,
            ),
            visibility = 10000,
            sys = Sys(
                type = 2,
                id = 2006176,
                country = "KE",
                sunrise = 1627789014,
                sunset = 1627832442
            ),
            weather = listOf(
                Weather(
                    id = 500,
                    main = "Clouds",
                    description = "broken clouds",
                    icon = "04n"
                )
            ),
            wind = Wind(
                speed = 2.62,
                deg = 99f,
            )
        )
    )

    val forecastResult: Result<ForecastResponse> = Result.Success(
        ForecastResponse(
            code = 200,
            cnt = 1,
            message = 0,
            city = City(
                id = 184745,
                name = "Nairobi",
                coord = Coordinate(
                    lat = -1.2833,
                    lon = 36.8167
                ),
                country = "KE",
                sunrise = 1627789030,
                sunset = 1627832449,
                timezone = 10800,
                population = 2750547
            ),
            list = listOf(
                ForecastWeather(
                    clouds = Cloud(all = 62),
                    dt = 1627849222,
                    main = Main(
                        temp = 16.41f,
                        feelsLike = 15.93f,
                        tempMin = 16.41f,
                        tempMax = 16.41f,
                        pressure = 1021,
                        humidity = 70,
                    ),
                    visibility = 10000,
                    sys = Sys(
                        type = 2,
                        id = 2006176,
                        country = "KE",
                        sunrise = 1627789014,
                        sunset = 1627832442
                    ),
                    weather = listOf(
                        Weather(
                            id = 500,
                            main = "Clouds",
                            description = "broken clouds",
                            icon = "04n"
                        )
                    ),
                    wind = Wind(
                        speed = 2.62,
                        deg = 99f,
                    ),
                    dtTxt = Date(),
                    pop = 0.0
                )
            )
        )
    )

    val currentWeather = remember {
        mutableStateOf(currentWeatherResult)
    }
    val forecastWeather = remember {
        mutableStateOf(forecastResult)
    }
    MainScreen(loading, currentWeather, forecastWeather) {

    }
}

@Preview(
    name = "Current Weather Screen",
    showSystemUi = true
)
@Composable
fun PreviewCurrentWeatherScreen() {
    val theme = remember {
        mutableStateOf(SUNNY)
    }
    val result: Result<CurrentWeather> = Result.Success(
        CurrentWeather(
            coord = Coordinate(36.8652, -1.221),
            name = "Kiambu",
            base = "",
            clouds = Cloud(all = 62),
            cod = 200,
            timezone = 10800,
            id = 192710,
            dt = 1627849222,
            main = Main(
                temp = 16.41f,
                feelsLike = 15.93f,
                tempMin = 16.41f,
                tempMax = 16.41f,
                pressure = 1021,
                humidity = 70,
            ),
            visibility = 10000,
            sys = Sys(
                type = 2,
                id = 2006176,
                country = "KE",
                sunrise = 1627789014,
                sunset = 1627832442
            ),
            weather = listOf(
                Weather(
                    id = 803,
                    main = "Clouds",
                    description = "broken clouds",
                    icon = "04n"
                )
            ),
            wind = Wind(
                speed = 2.62,
                deg = 99f,
            )
        )
    )

    val currentWeather = remember {
        mutableStateOf(result)
    }
    CurrentWeatherScreen(theme = theme, currentWeather)
}

@Preview(
    name = "Forecast Weather Screen",
    showSystemUi = true
)
@Composable
fun PreviewForecastScreen() {
    val forecastResult: Result<ForecastResponse> = Result.Success(
        ForecastResponse(
            code = 200,
            cnt = 1,
            message = 0,
            city = City(
                id = 184745,
                name = "Nairobi",
                coord = Coordinate(
                    lat = -1.2833,
                    lon = 36.8167
                ),
                country = "KE",
                sunrise = 1627789030,
                sunset = 1627832449,
                timezone = 10800,
                population = 2750547
            ),
            list = listOf(
                ForecastWeather(
                    clouds = Cloud(all = 62),
                    dt = 1627849222,
                    main = Main(
                        temp = 16.41f,
                        feelsLike = 15.93f,
                        tempMin = 16.41f,
                        tempMax = 16.41f,
                        pressure = 1021,
                        humidity = 70,
                    ),
                    visibility = 10000,
                    sys = Sys(
                        type = 2,
                        id = 2006176,
                        country = "KE",
                        sunrise = 1627789014,
                        sunset = 1627832442
                    ),
                    weather = listOf(
                        Weather(
                            id = 500,
                            main = "Clouds",
                            description = "broken clouds",
                            icon = "04n"
                        )
                    ),
                    wind = Wind(
                        speed = 2.62,
                        deg = 99f,
                    ),
                    dtTxt = Date(),
                    pop = 0.0
                ),
                ForecastWeather(
                    clouds = Cloud(all = 62),
                    dt = 1627849222,
                    main = Main(
                        temp = 16.41f,
                        feelsLike = 15.93f,
                        tempMin = 16.41f,
                        tempMax = 16.41f,
                        pressure = 1021,
                        humidity = 70,
                    ),
                    visibility = 10000,
                    sys = Sys(
                        type = 2,
                        id = 2006176,
                        country = "KE",
                        sunrise = 1627789014,
                        sunset = 1627832442
                    ),
                    weather = listOf(
                        Weather(
                            id = 500,
                            main = "Clouds",
                            description = "broken clouds",
                            icon = "04n"
                        )
                    ),
                    wind = Wind(
                        speed = 2.62,
                        deg = 99f,
                    ),
                    dtTxt = Date(),
                    pop = 0.0
                )
            )
        )
    )
    val forecastWeather = remember {
        mutableStateOf(forecastResult)
    }
    ForecastWeatherScreen(forecastWeather)
}

@Preview(
    name = "Day Forecast Row"
)
@Composable
fun PreviewDayForecastRow() {
    DayForecastRow(
        Date(),
        ForecastWeather(
            clouds = Cloud(all = 62),
            dt = 1627849222,
            main = Main(
                temp = 16.41f,
                feelsLike = 15.93f,
                tempMin = 16.41f,
                tempMax = 16.41f,
                pressure = 1021,
                humidity = 70,
            ),
            visibility = 10000,
            sys = Sys(
                type = 2,
                id = 2006176,
                country = "KE",
                sunrise = 1627789014,
                sunset = 1627832442
            ),
            weather = listOf(
                Weather(
                    id = 500,
                    main = "Clouds",
                    description = "broken clouds",
                    icon = "04n"
                )
            ),
            wind = Wind(
                speed = 2.62,
                deg = 99f,
            ),
            dtTxt = Date(),
            pop = 0.0
        )
    )
}
