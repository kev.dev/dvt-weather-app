package com.dvt.weather.ui.theme

import androidx.annotation.DrawableRes
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.dvt.weather.R

val SunnyThemeColors: Colors = lightColors(
    primary = Sunny,
    primaryVariant = Sunny,
    onPrimary = Color.White,
    secondary = Sunny,
    secondaryVariant = Sunny,
    onSecondary = Color.White,
    background = Sunny,
    onBackground = Color.White,
)

val CloudThemeColors: Colors = darkColors(
    primary = Cloudy,
    primaryVariant = Cloudy,
    onPrimary = Color.White,
    secondary = Cloudy,
    secondaryVariant = Cloudy,
    onSecondary = Color.White,
    background = Cloudy,
    onBackground = Color.White
)

val RainyThemeColors: Colors = darkColors(
    primary = Rainy,
    primaryVariant = Rainy,
    onPrimary = Color.White,
    secondary = Rainy,
    secondaryVariant = Rainy,
    onSecondary = Color.White,
    background = Rainy,
    onBackground = Color.White
)

val GeneralThemeColors: Colors = lightColors(
    primary = Sunny,
    primaryVariant = Sunny,
    onPrimary = Color.White,
    secondary = Sunny,
    secondaryVariant = Sunny,
    onSecondary = Color.White,
    background = Sunny,
    onBackground = Color.White,
)


@Composable
fun WeatherTheme(theme: Theme = Theme.SUNNY, content: @Composable() () -> Unit) {
    MaterialTheme(
        colors = theme.colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

@Composable
fun GeneralTheme(content: @Composable() () -> Unit) {
    MaterialTheme(
        colors = GeneralThemeColors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

enum class Theme(@DrawableRes var art: Int, var colors: Colors, var statusBarColor: Color) {
    SUNNY(R.drawable.forest_sunny, SunnyThemeColors, SunnyStatusBar),
    CLOUDY(R.drawable.forest_cloudy, CloudThemeColors, CloudyStatusBar),
    RAINY(R.drawable.forest_rainy, RainyThemeColors, RainyStatusBar)
}

val Themes: Array<Theme> = arrayOf(Theme.SUNNY, Theme.CLOUDY, Theme.RAINY)
