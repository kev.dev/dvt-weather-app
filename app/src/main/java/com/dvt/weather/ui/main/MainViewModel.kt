package com.dvt.weather.ui.main

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dvt.weather.data.entities.CurrentWeather
import com.dvt.weather.data.network.dto.ForecastResponse
import com.dvt.weather.domain.error.Failure
import com.dvt.weather.domain.interactor.CurrentWeatherInteractor
import com.dvt.weather.domain.interactor.Forecast5Interactor
import com.dvt.weather.domain.result.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    var currentWeatherInteractor: CurrentWeatherInteractor,
    var forecast5Interactor: Forecast5Interactor
) : ViewModel() {

    val currentWeather = MutableLiveData<Result<CurrentWeather>>()
    val loading = mutableStateOf(false)

    val forecast5 = MutableLiveData<Result<ForecastResponse>>()

    fun getCurrentWeather(lat: Double, lon: Double, units: String = "metric") {
        viewModelScope.launch {
            try {
                currentWeather.postValue(Result.Loading)
                loading.value = true
                val currentWeather = currentWeatherInteractor.run(
                    CurrentWeatherInteractor.Params(
                        lat = lat,
                        lon = lon,
                        units = units
                    )
                )
                this@MainViewModel.currentWeather.postValue(currentWeather)
                loading.value = false
            } catch (e: Exception) {
                Timber.e(e)
                loading.value = false
                currentWeather.postValue(Result.Error(Failure.GeneralFailure))
            }
        }
    }

    fun getForecast5(lat: Double, lon: Double, units: String = "metric") {
        viewModelScope.launch {
            try {
                forecast5.postValue(Result.Loading)
                val forecast = forecast5Interactor.run(
                    Forecast5Interactor.Params(
                        lat = lat,
                        lon = lon,
                        units = units
                    )
                )
                this@MainViewModel.forecast5.postValue(forecast)
            } catch (e: Exception) {
                Timber.e(e)
                forecast5.postValue(Result.Error(Failure.GeneralFailure))
            }
        }
    }
}
