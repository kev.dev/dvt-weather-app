package com.dvt.weather.ui.theme

import androidx.compose.ui.graphics.Color

val Sunny: Color = Color(0xFF47AB2F)
val SunnyStatusBar: Color = Color(0XFFFFD5A4)

val Cloudy: Color = Color(0xFF54717A)
val CloudyStatusBar: Color = Color(0XFF618495)

val Rainy: Color = Color(0xFF57575D)
val RainyStatusBar: Color = Color(0xFF57575D)
