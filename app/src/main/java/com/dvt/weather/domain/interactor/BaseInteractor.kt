package com.dvt.weather.domain.interactor

import com.dvt.weather.domain.result.Result

interface BaseInteractor<P, T> {
    suspend fun run(params: P): Result<T>
}
