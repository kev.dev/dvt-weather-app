package com.dvt.weather.domain.result

import com.dvt.weather.domain.error.Failure

sealed class Result<out T> {
    data class Success<out T>(val data: T, val cachedAt: Long? = null) : Result<T>()
    data class Error(val failure: Failure) : Result<Nothing>()
    object Loading : Result<Nothing>()
}
