package com.dvt.weather.domain.interactor

import com.dvt.weather.data.entities.CurrentWeather
import com.dvt.weather.data.repositories.WeatherRepository
import com.dvt.weather.domain.error.Failure
import com.dvt.weather.domain.result.Result
import javax.inject.Inject

class CurrentWeatherInteractor @Inject constructor(
    private var weatherRepository: WeatherRepository
) : BaseInteractor<CurrentWeatherInteractor.Params, CurrentWeather> {
    override suspend fun run(params: Params): Result<CurrentWeather> {
        return try {
            weatherRepository.currentWeather(
                lat = params.lat,
                lon = params.lon,
                units = params.units
            )
        } catch (e: Exception) {
            Result.Error(Failure.GeneralFailure)
        }
    }

    data class Params(var lat: Double, var lon: Double, var units: String)
}
