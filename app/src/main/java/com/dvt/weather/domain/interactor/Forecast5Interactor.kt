package com.dvt.weather.domain.interactor

import com.dvt.weather.data.network.dto.ForecastResponse
import com.dvt.weather.data.repositories.WeatherRepository
import com.dvt.weather.domain.error.Failure
import com.dvt.weather.domain.result.Result
import timber.log.Timber
import javax.inject.Inject

class Forecast5Interactor @Inject constructor(
    private var weatherRepository: WeatherRepository
) : BaseInteractor<Forecast5Interactor.Params, ForecastResponse> {
    override suspend fun run(params: Params): Result<ForecastResponse> {
        return try {
            weatherRepository.forecast5(
                lat = params.lat,
                lon = params.lon,
                units = params.units
            )
        } catch (e: Exception) {
            Result.Error(Failure.GeneralFailure)
        }
    }

    data class Params(var lat: Double, var lon: Double, var units: String)
}
