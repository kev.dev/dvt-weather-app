package com.dvt.weather.domain.error

sealed class Failure {
    object NetworkFailure : Failure()

    object GeneralFailure : Failure()
}
