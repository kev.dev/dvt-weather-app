package com.dvt.weather.data.entities

data class Coordinate(
    var lon: Double,
    var lat: Double
)
