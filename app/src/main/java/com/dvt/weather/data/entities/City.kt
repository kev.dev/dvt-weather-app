package com.dvt.weather.data.entities

data class City(
    var id: Long,
    var name: String,
    var coord: Coordinate,
    var country: String,
    var population: Int,
    var timezone: Long,
    var sunrise: Long,
    var sunset: Long
)
