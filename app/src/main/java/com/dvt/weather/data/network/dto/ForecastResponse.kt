package com.dvt.weather.data.network.dto

import com.dvt.weather.data.entities.City
import com.dvt.weather.data.entities.ForecastWeather

data class ForecastResponse(
    var code: Int,
    var message: Int,
    var cnt: Int,
    var list: List<ForecastWeather>,
    var city: City
)
