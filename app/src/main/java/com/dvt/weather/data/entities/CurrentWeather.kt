package com.dvt.weather.data.entities

data class CurrentWeather(
    var coord: Coordinate,
    var weather: List<Weather>,
    var base: String,
    var main: Main,
    var visibility: Int,
    var wind: Wind,
    var clouds: Cloud,
    var dt: Long,
    var sys: Sys,
    var timezone: Long,
    var id: Long,
    var name: String,
    var cod: Int
)
