package com.dvt.weather.data.entities

data class CachedCurrentWeather(
    var currentWeather: CurrentWeather,
    var cachedAt: Long
)