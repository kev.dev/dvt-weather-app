package com.dvt.weather.data.entities

data class Wind(
    var speed: Double,
    var deg: Float
)
