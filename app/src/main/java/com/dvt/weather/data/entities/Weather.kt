package com.dvt.weather.data.entities

data class Weather(
    var id: Long,
    var main: String,
    var description: String,
    var icon: String
) {
    fun isRainy(): Boolean {
        return IntRange(200, 233).contains(id)
                || IntRange(300, 321).contains(id)
                || IntRange(500, 531).contains(id)
    }

    fun isCloudy(): Boolean {
        return IntRange(801, 804).contains(id)
    }

    fun isSunny(): Boolean {
        return id == 800L
    }

    fun name(): String {
        return when {
            isRainy() -> "RAINY"
            isSunny() -> "SUNNY"
            isCloudy() -> "CLOUDY"
            else -> ""
        }
    }
}
