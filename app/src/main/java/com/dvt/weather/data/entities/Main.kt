package com.dvt.weather.data.entities

data class Main(
    var temp: Float,
    var feelsLike: Float,
    var tempMin: Float,
    var tempMax: Float,
    var pressure: Int,
    var humidity: Int
)
