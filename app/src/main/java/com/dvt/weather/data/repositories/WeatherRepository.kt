package com.dvt.weather.data.repositories

import com.dvt.weather.BuildConfig
import com.dvt.weather.data.entities.*
import com.dvt.weather.data.network.dto.ForecastResponse
import com.dvt.weather.data.network.services.OpenWeatherService
import com.dvt.weather.data.queries.WeatherQueries
import com.dvt.weather.domain.error.Failure
import com.dvt.weather.domain.result.Result
import timber.log.Timber
import java.net.UnknownHostException
import javax.inject.Inject

class WeatherRepository @Inject constructor(
    private var openWeatherService: OpenWeatherService,
    private var weatherQueries: WeatherQueries
) {
    suspend fun currentWeather(
        lat: Double,
        lon: Double,
        units: String = "metric"
    ): Result<CurrentWeather> {
        return try {
            val response = openWeatherService.current(
                lat,
                lon,
                BuildConfig.OPENWEATHER_API_KEY,
                units
            )
            return if (response.isSuccessful) {
                val currentWeather = response.body()!!
                saveToLocal(currentWeather = currentWeather, units, "current")
                Result.Success(currentWeather)
            } else {
                val local = currentFromLocal()
                return if (local is Result.Success) {
                    local
                } else {
                    Result.Error(Failure.NetworkFailure)
                }
            }
        } catch (e: UnknownHostException) {
            val local = currentFromLocal()
            return if (local is Result.Success) {
                local
            } else {
                Result.Error(Failure.NetworkFailure)
            }
        }
    }

    private fun saveToLocal(
        currentWeather: CurrentWeather,
        units: String,
        type: String = "current"
    ) {
        try {
            val weather = currentWeather.weather[0]
            weatherQueries.insert(
                com.dvt.weather.data.queries.Weather(
                    sunrise = currentWeather.sys.sunrise,
                    sunset = currentWeather.sys.sunset,
                    country = currentWeather.sys.country,
                    id = 0,
                    units = units,
                    type = type,
                    humidity = currentWeather.main.humidity.toLong(),
                    pressure = currentWeather.main.pressure.toLong(),
                    feels_like = currentWeather.main.feelsLike.toDouble(),
                    temp = currentWeather.main.temp.toDouble(),
                    temp_min = currentWeather.main.temp.toDouble(),
                    temp_max = currentWeather.main.temp.toDouble(),
                    timezone = currentWeather.timezone,
                    clouds_all = currentWeather.clouds.all.toLong(),
                    date = currentWeather.dt,
                    updated_at = System.currentTimeMillis(),
                    location_lat = currentWeather.coord.lat,
                    location_lon = currentWeather.coord.lat,
                    location_name = currentWeather.name,
                    weather_description = weather.description,
                    weather_icon = weather.icon,
                    weather_id = weather.id,
                    weather_main = weather.main,
                )
            )
        } catch (e: Exception) {
            Timber.e(e)
            e.printStackTrace()
        }
    }

    private fun currentFromLocal(): Result<CurrentWeather> {
        return try {
            val local = weatherQueries.findLastCurrent(
                type_ = "current",
                mapper = { id, type, weather_id, weather_main, weather_description, weather_icon, units, country, location_name, location_lat, location_lon, feels_like, temp, temp_max, temp_min, pressure, humidity, date, timezone, sunrise, sunset, clouds_all, updated_at ->
                    val currentWeather = CurrentWeather(
                        coord = Coordinate(location_lat, location_lon),
                        name = location_name,
                        base = "",
                        clouds = Cloud(all = clouds_all.toInt()),
                        cod = 200,
                        timezone = timezone,
                        id = 0,
                        dt = date,
                        main = Main(
                            temp = temp.toFloat(),
                            feelsLike = feels_like.toFloat(),
                            tempMin = temp_min.toFloat(),
                            tempMax = temp_max.toFloat(),
                            pressure = pressure.toInt(),
                            humidity = humidity.toInt(),
                        ),
                        visibility = 10000,
                        sys = Sys(
                            type = 2,
                            id = -1,
                            country = country,
                            sunrise = sunrise,
                            sunset = sunset
                        ),
                        weather = listOf(
                            Weather(
                                id = weather_id,
                                main = "Clouds",
                                description = "broken clouds",
                                icon = "04n"
                            )
                        ),
                        wind = Wind(
                            speed = 2.62,
                            deg = 99f,
                        )
                    )
                    CachedCurrentWeather(
                        currentWeather = currentWeather,
                        cachedAt = updated_at
                    )
                }).executeAsOne()
            return Result.Success(local.currentWeather, local.cachedAt)
        } catch (e: Exception) {
            Result.Error(Failure.GeneralFailure)
        }
    }

    suspend fun forecast5(
        lat: Double,
        lon: Double,
        units: String = "metric"
    ): Result<ForecastResponse> {
        val response = openWeatherService.forecast(
            lat,
            lon,
            BuildConfig.OPENWEATHER_API_KEY,
            units
        )
        return if (response.isSuccessful) {
            Result.Success(response.body()!!)
        } else {
            Result.Error(Failure.NetworkFailure)
        }
    }
}
