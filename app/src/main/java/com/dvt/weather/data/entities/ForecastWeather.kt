package com.dvt.weather.data.entities

import java.util.*

data class ForecastWeather(
    var dt: Long,
    var main: Main,
    var weather: List<Weather>,
    var clouds: Cloud,
    var wind: Wind,
    var visibility: Int,
    var pop: Double,
    var sys: Sys,
    var dtTxt: Date
)
